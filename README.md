# Vinhos 

# Live demo 
[Demonstração no heroku app](https://winestore.herokuapp.com/)

# API Endpoints:

## clientes/
    - Lista todos os clientes
## clientes/fieis
    - Lista todos os clientes ordenados por frequencia de compras

## clientes/compras/total
    - Lista todos os clientes ordenados pelo valor total de compras

## clientes/compra/total
    - Lista todos os clientes ordenados pela compra que possui maior valor em compras

## clientes/compra/maior
    - Retorna um cliente com o maior valor de compra do ultimo ano retornado

## compras/
    - Lista todas as compras

## itens/recomendacao/{id}
    -Retorna uma recomendacao de item para um cliente especifico escolhido pelo id


# Requirements
    - Java 1.8 +
    - Maven 3.0 +
    - Spring Boot 2.0.7.BUILD-SNAPSHOT

# Run
    - mvn dependency:tree 
    - mvn spring-boot:run
# Testando a API com Collection Runner do Postman

1) Importar o arquivo `ProvaMonicaAPI.postman_collection.json`



![1](docs/images/1.png)

![2](docs/images/2.png)

![3](docs/images/3.png)

2) Setar a vari�vel `url` em um ambiente com o host e a porta em que a aplica��o est� rodando

![4](docs/images/4.png)

![5](docs/images/5.png)



3) Rodar os testes utilizando Collection Runner no ambiente criado

![6](docs/images/6.png)

![7](docs/images/7.png)
