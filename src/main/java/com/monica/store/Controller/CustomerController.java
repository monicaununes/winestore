package com.monica.store.Controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.ws.http.HTTPException;

import com.monica.store.model.Customer;
import com.monica.store.model.Purchase;
import com.monica.store.resource.CustomerService;
import com.monica.store.resource.ItemService;
import com.monica.store.resource.PurchaseService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clientes")
public class CustomerController {

    PurchaseService purchaseService = new PurchaseService();
    List<Purchase> purchases = purchaseService.getPurchases();
    CustomerService customerService = new CustomerService();
    List<Customer> customers = customerService.getCustomers();
    ItemService itemService = new ItemService();

    @RequestMapping(value = { "", "/" })
    public List<Customer> customers() throws HTTPException {
        return customerService.getCustomers();
    }

    @RequestMapping("/fieis")
    public List<Customer> loyalCustomers() throws HTTPException {
        Map<String, List<Purchase>> customerPurchase = purchases.stream()
                .collect(Collectors.groupingBy(Purchase::getCustomer));
        Map<String, Integer> size = new HashMap<String, Integer>();
        for (Purchase purchase : purchases) {
            size.put(purchase.getCustomer(), customerPurchase.get(purchase.getCustomer()).size());
        }
        size = size.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new));
        List<Customer> customersBySize = customerService.customersOrderBySize(size, customers);
        return customersBySize;
    }

    @RequestMapping("/compras/total")
    public List<Customer> amount() throws HTTPException {
        Map<String, List<Purchase>> customerPurchase = purchases.stream()
                .collect(Collectors.groupingBy(Purchase::getCustomer));
        List<Purchase> purchases = new ArrayList<Purchase>();
        Map<String, Double> amount = new HashMap<String, Double>();
        List<Customer> customersList = new ArrayList<Customer>();
        for (Customer customer : customers) {
            purchases = customerPurchase.get(customer.getCpf());
            amount.put(customer.getCpf(), purchases.stream().mapToDouble(Purchase::getAmount).sum());
        }
        amount = amount.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new));
        for (Map.Entry<String, Double> amountCustomer : amount.entrySet()) {
            customersList.add(customerService.getCustomerByCpf(customers, amountCustomer.getKey()));
        }
        return customersList;
    }

    @RequestMapping("/compra/total")
    public List<Customer> purchaseAmount() throws HTTPException {
        purchases.sort(Comparator.comparing(Purchase::getAmount).reversed());
        return customerService.customersOrderByPurchase(customers, purchases);
    }

    @RequestMapping("/compras/maior")
    public Customer biggestPurchase() throws HTTPException {
        purchases.sort(Comparator.comparing(Purchase::getData).reversed());
        List<Purchase> currentPurchases = purchaseService
                .getCurrentPurchases(purchases.get(0).getData().get(Calendar.YEAR), purchases);
        currentPurchases.sort(Comparator.comparing(Purchase::getAmount).reversed());
        List<Customer> customer = customerService.customersOrderByPurchase(customers, currentPurchases);
        return customer.get(0);
    }
}