package com.monica.store.Controller;

import java.util.Comparator;
import java.util.List;

import javax.xml.ws.http.HTTPException;

import com.monica.store.model.Customer;
import com.monica.store.model.Purchase;
import com.monica.store.model.Item;
import com.monica.store.resource.CustomerService;
import com.monica.store.resource.PurchaseService;
import com.monica.store.resource.ItemService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/itens")
public class ItemsController {

    PurchaseService purchaseService = new PurchaseService();
    List<Purchase> purchases = purchaseService.getPurchases();
    CustomerService customerService = new CustomerService();
    List<Customer> customers = customerService.getCustomers();
    ItemService itemService = new ItemService();

    @RequestMapping("/recomendacao/{id}")
    public Item recommendation(@PathVariable int id) throws HTTPException {
        Customer customer = customerService.getCustomerById(customers, id);
        List<Item> itemCustomer = itemService.itemsByCustomer(purchases, customer);
        itemCustomer.sort(Comparator.comparing(Item::getProduct).thenComparing(Item::getVariety));
        List<Item> items = itemService.productVarietyRecommendation(itemCustomer, customer);
        items.sort(Comparator.comparing(Item::getQuantity).reversed());
        return items.get(0);
    }

}