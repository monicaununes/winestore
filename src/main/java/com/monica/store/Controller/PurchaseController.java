package com.monica.store.Controller;

import java.util.List;

import javax.xml.ws.http.HTTPException;

import com.monica.store.model.Purchase;
import com.monica.store.resource.PurchaseService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PurchaseController {

    @RequestMapping("/compras")
    public List<Purchase> purchase() throws HTTPException {
        PurchaseService purchasesService = new PurchaseService();
        return purchasesService.getPurchases();
    }
}