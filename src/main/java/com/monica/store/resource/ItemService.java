package com.monica.store.resource;

import java.util.ArrayList;
import java.util.List;

import com.monica.store.model.Customer;
import com.monica.store.model.Purchase;
import com.monica.store.model.Item;

import org.springframework.stereotype.Service;

@Service
public class ItemService {
    public List<Item> itemsByCustomer(List<Purchase> purchases, Customer customer) {
        List<Item> customerItem = new ArrayList<Item>();
        for (Purchase purchase : purchases) {
            if (customer.getCpf().equals(purchase.getCustomer())) {
                for (Item item : purchase.getItems()) {
                    customerItem.add(item);
                }
            }
        }
        return customerItem;
    }

    public List<Item> productVarietyRecommendation(List<Item> customerItem, Customer customer) {
        List<Item> items = new ArrayList<Item>();
        Item option = new Item();
        for (int i = 0; i < customerItem.size(); i++) {
            Item item = customerItem.get(i);
            if (option.getVariety() != null && option.getProduct() != null
                    && item.getProduct().equals(option.getProduct()) && item.getVariety().equals(option.getVariety())) {
                option.setQuantity(option.getQuantity() + 1);
            } else {
                if (option.getVariety() != null && option.getProduct() != null) {
                    items.add(option);
                }
                option = new Item(item.getId(), item.getProduct(), item.getVariety(), item.getCountry(),
                        item.getCategory(), item.getHarvest(), item.getPrice(), 1);
            }
            if (i + 1 == customerItem.size()) {
                items.add(option);
            }
        }
        return items;
    }
}