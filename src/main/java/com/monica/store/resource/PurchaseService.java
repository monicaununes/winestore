package com.monica.store.resource;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.monica.store.model.Purchase;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PurchaseService {
    private static String getPurchasesEndpoint = "http://www.mocky.io/v2/598b16861100004905515ec7";
    public List<Purchase> getPurchases() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<List<Purchase>> response = restTemplate.exchange(
                getPurchasesEndpoint, HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<Purchase>>() {
                });
        List<Purchase> purchases = response.getBody();
        return purchases;
    }

    public List<Purchase> getCurrentPurchases(int year, List<Purchase> purchases) {
        List<Purchase> currentPurchases = new ArrayList<Purchase>();
        for (Purchase purchase : purchases) {
            if (purchase.getData().get(Calendar.YEAR) == year) {
                currentPurchases.add(purchase);
            }
        }
        return currentPurchases;
    }
}