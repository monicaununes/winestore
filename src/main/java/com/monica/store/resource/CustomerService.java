package com.monica.store.resource;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.monica.store.model.Purchase;
import com.monica.store.model.Customer;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CustomerService {
    private static String getCustomersEndpoint = "http://www.mocky.io/v2/598b16291100004705515ec5";
    public List<Customer> getCustomers() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        ResponseEntity<List<Customer>> response = restTemplate.exchange(
                getCustomersEndpoint, HttpMethod.GET, entity,
                new ParameterizedTypeReference<List<Customer>>() {
                });
        List<Customer> customers = response.getBody();
        return customers;
    }

    public List<Customer> customersOrderBySize(Map<String, Integer> size, List<Customer> customers) {
        List<Customer> customersBySize = new ArrayList<Customer>();
        for (Map.Entry<String, Integer> fiel : size.entrySet()) {
            for (Customer customer : customers) {
                if (fiel.getKey().equals(customer.getCpf())) {
                    customersBySize.add(customer);
                }
            }
        }
        return customersBySize;
    }

    public List<Customer> customersOrderByPurchase(List<Customer> customers, List<Purchase> purchases) {
        List<Customer> customerOrderByPurchase = new ArrayList<Customer>();
        for (Purchase purchase : purchases) {
            for (Customer customer : customers) {
                if (purchase.getCustomer().equals(customer.getCpf())) {
                    if (!customerOrderByPurchase.contains(customer)) {
                        customerOrderByPurchase.add(customer);
                    }
                }
            }
        }
        return customerOrderByPurchase;
    }

    public Customer getCustomerById(List<Customer> customers, int id) {
        Customer customerByName = new Customer();
        for (Customer customer : customers) {
            if (customer.getId() == id) {
                customerByName = customer;
                return customerByName;
            }
        }
        return customerByName;
    }

    public Customer getCustomerByCpf(List<Customer> customers, String cpf) {
        Customer customerByCpf = new Customer();
        for (Customer customer : customers) {
            if (customer.getCpf().equals(cpf)) {
                customerByCpf = customer;
                return customerByCpf;
            }
        }
        return customerByCpf;
    }
}