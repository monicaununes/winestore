package com.monica.store.utils;

import java.text.ParseException;

import javax.swing.text.MaskFormatter;

public class StringUtils {

    public static String applyMask(String value, String pattern) {
        try {
            MaskFormatter mf = new MaskFormatter(pattern);
            mf.setValidCharacters("0123456789");
            mf.setValueContainsLiteralCharacters(false);
            return mf.valueToString(value);
        } catch (ParseException ex) {
            return value;
        }
    }
}