package com.monica.store.model;

import java.util.GregorianCalendar;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.monica.store.utils.StringUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Purchase {

    @JsonProperty("codigo")
    private String id;
    @JsonProperty("data")
    @JsonFormat(pattern = "dd-MM-yyyy")
    private GregorianCalendar date;
    @JsonProperty("cliente")
    private String customer;
    @JsonProperty("itens")
    private List<Item> items;
    @JsonProperty("valorTotal")
    private Double amount;
    
    public String getCodigo() {
        return this.id;
    }
    
    public GregorianCalendar getData() {
        return this.date;
    }

    public String getCustomer() {
        this.customer = String.format("%1$" + 15 + "s", this.customer).replace(' ', '0').replace("-", "").replace(".",
                "");
        this.customer = StringUtils.applyMask(this.customer, "####.###.###-##");
        return this.customer;
    }

    public List<Item> getItems() {
        return this.items;
    }

    public Double getAmount() {
        return this.amount;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDate(GregorianCalendar date) {
        this.date = date;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}