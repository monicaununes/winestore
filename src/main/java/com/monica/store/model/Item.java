package com.monica.store.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {

    @JsonProperty("codigo")
    private String id;
    @JsonProperty("produto")
    private String product;
    @JsonProperty("variedade")
    private String variety;
    @JsonProperty("pais")
    private String country;
    @JsonProperty("categoria")
    private String category;
    @JsonProperty("safra")
    private String harvest;
    @JsonProperty("preco")
    private Double price;
    @JsonIgnore
    private int quantity;

    public Item(){

    }
    
    public Item(String id, String product, String variety, String country, String category, String harvest, Double price, int quantity){
        this.id = id;
        this.product = product;
        this.variety = variety;
        this.country = country;
        this.category = category;
        this.harvest = harvest;
        this.price = price;    
        this.quantity = quantity;
    }

    public String getId() {
        return this.id;
    }

    public String getProduct() {
        return this.product;
    }

    public String getVariety() {
        return this.variety;
    }

    public String getCountry() {
        return this.country;
    }

    public String getCategory() {
        return this.category;
    }

    public String getHarvest() {
        return this.harvest;
    }

    public Double getPrice() {
        return this.price;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setHarvest(String harvest) {
        this.harvest = harvest;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}