package com.monica.store.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer {

    private int id;
    @JsonProperty("nome")
    private String name;
    private String cpf;

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getCpf() {
        this.cpf = String.format("%1$" + 15 + "s", this.cpf).replace(' ', '0');
        return this.cpf;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}